dataset= read.csv('dataset.csv')

# Research question 1
x <- dataset$quality
y <- dataset$pH

plot(x,y)
md <- lm (y ~ x, data = dataset)
abline (md, col = "red")

h <-hist(y, freq = F, xlab="pH", ylab="frequency", main="Histogram of quality")
lines(density(y,adjust=2), col = "red")
model1= cor.test(x,y, method = "pearson")
model1

#The scatter plot with linear model shows that the pH for the quality. The result from the correlation test showed the correlation between pH and quality is negative (-0.577) at 95 % confidence level (-0.106451268, -0.008734972). P-value=0.02096 < 0.05 therefore, the null hypothesis  can be rejected and we can suggest  there is a correlation between pH and quality of wine.

# Research question 2
x <- dataset$quality
y <- dataset$volatile.acidity

plot(x,y)
md <- lm (y ~ x, data = dataset)
abline (md, col = "red")

h <-hist(y, freq = F, xlab="volatile.acidity", ylab="frequency", main="Histogram of quality")
lines(density(y,adjust=2), col = "red")

model2= cor.test(x,y, method = "pearson")
model2

# Research question 3
x <- dataset$quality
y <- dataset$chlorides

plot(x,y)
md <- lm (y ~ x, data = dataset)
abline (md, col = "red")

h <-hist(y, freq = F, xlab="chlorides", ylab="frequency", main="Histogram of quality")
lines(density(y,adjust=2), col = "red")
model3= cor.test(x,y, method = "pearson")
model3

# Research question 4
x <- dataset$quality
y <- dataset$alcohol

plot(x,y)
md <- lm (y ~ x, data = dataset)
abline (md, col = "red")

h <-hist(y, freq = F, xlab="alcohol", ylab="frequency", main="Histogram of quality")
lines(density(y,adjust=2), col = "red")
model4= cor.test(x,y, method = "pearson")
model4
# Research question 5
x <- dataset$quality
y <- dataset$density

plot(x,y)
md <- lm (y ~ x, data = dataset)
abline (md, col = "red")

h <-hist(y, freq = F, xlab="density", ylab="frequency", main="Histogram of quality")
lines(density(y,adjust=2), col = "red")
model5= cor.test(x,y, method = "pearson")
model5
# Research question 6
modl6= lm(quality~ density+chlorides+alcohol+pH+volatile.acidity, data= dataset)
summary(modl6)

